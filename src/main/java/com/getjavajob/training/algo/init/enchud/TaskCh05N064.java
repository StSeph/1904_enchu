package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh05N064 {

    private static int NUMBER_OF_AREAS = 12;

    public static void main(String[] args) {
        double[] people = countAllPeople();
        double[] space = countAllSpace();
        double populationDensity = countPopulationDensity(people, space);
        System.out.println("Average population density in the region is " + populationDensity + " people per square kilometer");
    }

    public static double[] countAllPeople() {
        double[] peopleInEachAreas = new double[NUMBER_OF_AREAS];
        Scanner inputPeople = new Scanner(System.in);
        for (int i = 0; i < NUMBER_OF_AREAS; i++) {
            System.out.println("How many thousands live in " + (i + 1) + " area");
            double peopleInOneArea = inputPeople.nextDouble();
            peopleInEachAreas[i] = peopleInOneArea * 1000;
        }
        return peopleInEachAreas;
    }

    public static double[] countAllSpace() {
        double[] areaSpace = new double[NUMBER_OF_AREAS];
        Scanner inputSpace = new Scanner(System.in);
        for (int i = 0; i < NUMBER_OF_AREAS; i++) {
            System.out.println("Enter the space of " + (i + 1) + " area");
            double oneAreaSpace = inputSpace.nextDouble();
            areaSpace[i] = oneAreaSpace;
        }
        return areaSpace;
    }

    public static double countPopulationDensity(double[] people, double[] space) {
        double population = 0;
        for (int i = 0; i < NUMBER_OF_AREAS; i++) {
            population += people[i];
        }
        double land = 0;
        for (int i = 0; i < NUMBER_OF_AREAS; i++) {
            land += space[i];
        }
        return population / land;
    }
}