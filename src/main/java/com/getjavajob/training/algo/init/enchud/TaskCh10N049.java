package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N049 {

    public static void main(String[] args) {
        System.out.println("Enter the number of elements in the array");
        Scanner inputNumber = new Scanner(System.in);
        int amountOfNumbers = inputNumber.nextInt();
        int[] numbers = fillTheArray(amountOfNumbers);
        int biggestElemOfNumbers = foundBiggerElem(numbers, amountOfNumbers);
        int indexOfBiggestElement = findIndexOfBiggestElement(numbers, biggestElemOfNumbers, amountOfNumbers);
        System.out.println("The index of the array element with the maximum value is " + indexOfBiggestElement);
    }

    public static int[] fillTheArray(int amountOfNumbers) {
        int[] emptyArray = new int[amountOfNumbers];
        System.out.println("Fill the array");
        Scanner inputValue = new Scanner(System.in);
        for (int i = 0; i < amountOfNumbers; i++)
            emptyArray[i] = inputValue.nextInt();
        return emptyArray;
    }

    public static int foundBiggerElem(int[] numbers, int amountOfNumbers) {
        if (amountOfNumbers - 1 >= 0) {
            int numberInArray = foundBiggerElem(numbers, amountOfNumbers - 1);
            return (numbers[amountOfNumbers - 1] > numberInArray) ? numbers[amountOfNumbers - 1] : numberInArray;
        } else {
            return numbers[0];
        }
    }

    public static int findIndexOfBiggestElement(int[] numbers, int biggestElemOfNumbers, int amountOfNumbers) {
        if (amountOfNumbers - 1 >= 0) {
            return (numbers[amountOfNumbers - 1] == biggestElemOfNumbers) ? amountOfNumbers - 1 : findIndexOfBiggestElement(numbers, biggestElemOfNumbers, amountOfNumbers - 1);
        } else {
            return 0;
        }
    }
}