package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N043 {

    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        int sumOfDigits = countSumDigits(number);
        int quantityOfDigits = countQuantityDigits(number);
        System.out.println("Sum of all numbers is " + sumOfDigits);
        System.out.println("There are " + quantityOfDigits + " numbers in your number");
    }

    public static int countSumDigits(int number) {
        if (number > 0) {
            return number % 10 + countSumDigits(number / 10);
        }
        return number;
    }

    public static int countQuantityDigits(int number) {
        if (number > 0) {
            return 1 + countQuantityDigits(number / 10);
        }
        return number;
    }
}