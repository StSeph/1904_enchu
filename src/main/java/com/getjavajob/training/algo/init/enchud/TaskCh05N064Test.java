package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh05N064.countPopulationDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {

    public static void main(String[] args) {
        testCountPopulationDensity();
    }

    public static void testCountPopulationDensity() {
        double[] testPeopleInArea = {1000, 1000, 1000, 1000, 1000, 1000, 10000, 10000, 10000, 10000, 10000, 10000};
        double[] testAreaSpace = {1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2};
        assertEquals("TaskCh05N064Test.testCountPopulationDensity", 3666.6666666666665, countPopulationDensity(testPeopleInArea, testAreaSpace));
    }
}