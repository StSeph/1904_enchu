package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh12N063.countAverageInYearClass;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {

    public static void main(String[] args) {
        testCountAveragePupils();
    }

    public static void testCountAveragePupils() {
        int[][] testInformationOfPupils = new int[][]{
                {16, 25, 33, 46}, // 1 class average 30
                {52, 43, 45, 54}, // 2 class average 48
                {30, 23, 24, 26}, // 3 class average 25
                {26, 26, 24, 22}, // 4 class average 24
                {25, 16, 18, 22}, // 5 class average 20
                {26, 19, 23, 33}, // 6 class average 25
                {21, 22, 23, 43}, // 7 class average 27
                {22, 18, 32, 19}, // 8 class average 22
                {30, 29, 21, 27}, // 9 class average 26
                {12, 19, 15, 13}, // 10 class average 14
                {41, 41, 43, 45}, // 11 class average 42
        };
        assertEquals("TaskCh12N063Test.testCountAveragePupils", 14, countAverageInYearClass(testInformationOfPupils, 9));
    }
}