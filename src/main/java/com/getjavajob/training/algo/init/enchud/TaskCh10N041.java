package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N041 {

    public static void main(String[] args) {
        System.out.println("Enter the number, which factorial should be found");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        int factorial = countFactorial(number);
        System.out.println(factorial);
    }

    public static int countFactorial(int number) {
        if (number == 0) {
            return 1;
        }
        return number * countFactorial(number - 1);
    }
}