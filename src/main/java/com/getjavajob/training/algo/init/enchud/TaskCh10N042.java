package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N042 {

    public static void main(String[] args) {
        System.out.println("What number to raise to a power?");
        Scanner inputNumber = new Scanner(System.in);
        int a = inputNumber.nextInt();
        System.out.println("To what extent does your number grow?");
        int n = inputNumber.nextInt();
        int result = countNumberInDegree(a, n);
        System.out.println(result);
    }

    public static int countNumberInDegree(int number, int degree) {
        if (number > 0) {
            return number * countNumberInDegree(number, degree - 1);
        }
        return number;
    }
}