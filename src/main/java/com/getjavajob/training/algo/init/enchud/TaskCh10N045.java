package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N045 {

    public static void main(String[] args) {
        System.out.println("Enter the first member of the arithmetic progression");
        Scanner inputNumber = new Scanner(System.in);
        int firstNumber = inputNumber.nextInt();
        System.out.println("Enter the difference of the arithmetic progression");
        int difference = inputNumber.nextInt();
        System.out.println("Enter the step number you need");
        int step = inputNumber.nextInt();
        int neededStep = countStepNumber(firstNumber, difference, step);
        int sumStepNumbers = countSumOfNumbers(firstNumber, difference, step);
        System.out.println(step + " progression member is " + neededStep);
        System.out.println("The sum of the first members of the progression is  " + sumStepNumbers);
    }

    public static int countStepNumber(int firstNumber, int difference, int step) {
        if (step == 0) {
            return firstNumber;
        }
        return firstNumber + difference + countStepNumber(0, difference, step - 1);
    }

    public static int countSumOfNumbers(int firstNumber, int difference, int step) {
        if (step == 0) {
            return firstNumber;
        }
        return firstNumber + difference * step + countSumOfNumbers(firstNumber, difference, step - 1);
    }
}