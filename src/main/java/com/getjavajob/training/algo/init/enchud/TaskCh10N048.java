package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N048 {

    public static void main(String[] args) {
        System.out.println("Enter the number of elements in the array");
        Scanner inputNumber = new Scanner(System.in);
        int amountOfNumbers = inputNumber.nextInt();
        int[] numbers = fillTheArray(amountOfNumbers);
        int biggestElemOfMatrix = foundBiggerElem(numbers, amountOfNumbers);
        System.out.println("Maximum value of the array element is " + biggestElemOfMatrix);
    }

    public static int[] fillTheArray(int amountOfNumbers) {
        int[] emptyArray = new int[amountOfNumbers];
        System.out.println("Fill the array");
        Scanner inputValue = new Scanner(System.in);
        for (int i = 0; i < emptyArray.length; i++) {
            emptyArray[i] = inputValue.nextInt();
        }
        return emptyArray;
    }

    public static int foundBiggerElem(int[] numbers, int amountOfNumbers) {
        if (amountOfNumbers - 1 >= 0) {
            int numberInArray = foundBiggerElem(numbers, amountOfNumbers - 1);
            return (numbers[amountOfNumbers - 1] > numberInArray) ? numbers[amountOfNumbers - 1] : numberInArray;
        } else {
            return numbers[0];
        }
    }
}