package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh05N010.exchangeDollars;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N010Test {

    public static void main(String[] args) {
        firstTestExchangeDollars();
        secondTestExchangeDollars();
    }

    public static void firstTestExchangeDollars() {
        double[] testFirstArray = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200};
        assertEquals("TaskCh05N010Test.testFirstArray", testFirstArray, exchangeDollars(10));
    }

    public static void secondTestExchangeDollars() {
        double[] testSecondArray = {60, 120, 180, 240, 300, 360, 420, 480, 540, 600, 660, 720, 780, 840, 900, 960, 1020, 1080, 1140, 1200};
        assertEquals("TaskCh05N010Test.testSecondArray", testSecondArray, exchangeDollars(60));
    }
}