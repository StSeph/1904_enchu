package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N166 {

    public static void main(String[] args) {
        Scanner inputString = new Scanner(System.in);
        String enteredString = inputString.nextLine();
        String changedSentence = replaceFirstAndLastWord(enteredString);
        System.out.println(changedSentence);
    }

    public static String replaceFirstAndLastWord(String enteredString) {
        String[] wordsFromSentence = enteredString.split(" ");
        String firstWord = wordsFromSentence[0];
        wordsFromSentence[0] = wordsFromSentence[wordsFromSentence.length - 1];
        wordsFromSentence[wordsFromSentence.length - 1] = firstWord;
        return String.join(" ", wordsFromSentence);
    }
}