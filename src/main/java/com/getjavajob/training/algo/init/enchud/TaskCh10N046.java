package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N046 {

    public static void main(String[] args) {
        System.out.println("Enter the first term of the geometric progression");
        Scanner inputNumber = new Scanner(System.in);
        int firstNumber = inputNumber.nextInt();
        System.out.println("Enter the denominator of the geometric progression");
        int denominator = inputNumber.nextInt();
        System.out.println("Enter the step number you need");
        int step = inputNumber.nextInt();
        int neededStep = countStepNumber(firstNumber, denominator, step);
        int sumStepNumbers = countSumOfNumbers(firstNumber, denominator, step);
        System.out.println(step + " progression member is " + neededStep);
        System.out.println("The sum of the first members of the progression is " + sumStepNumbers);
    }

    public static int countStepNumber(int firstNumber, int denominator, int step) {
        if (step == 0) {
            return firstNumber;
        }
        return firstNumber * denominator * countStepNumber(1, denominator, step - 1);
    }

    public static int countSumOfNumbers(int firstNumber, int denominator, int step) {
        if (step == 0) {
            return firstNumber;
        }
        return (int) (firstNumber * Math.pow(denominator, step) + countSumOfNumbers(firstNumber, denominator, step - 1));
    }
}