package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N048.foundBiggerElem;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {

    public static void main(String[] args) {
        testFoundBiggerElem();
    }

    public static void testFoundBiggerElem() {
        int[] testNumbers = {5, 14, 3323, 124, 568};
        assertEquals("TaskCh10N048Test.testFoundBiggerElem", 3323, foundBiggerElem(testNumbers, 5));
    }
}