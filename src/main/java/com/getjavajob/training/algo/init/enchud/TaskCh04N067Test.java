package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh04N067.checkWorkdayOrNot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {

    public static void main(String[] args) {
        testIsWorkday();
        testIsWeekend();
    }

    private static void testIsWeekend() {
        assertEquals("TaskCh04N067Test.testIsWorkday", "Weekend", checkWorkdayOrNot(7));
    }

    public static void testIsWorkday() {
        assertEquals("TaskCh04N067Test.testIsWorkday", "Workday", checkWorkdayOrNot(5));
    }
}