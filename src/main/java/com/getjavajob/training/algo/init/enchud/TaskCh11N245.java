package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

import static java.lang.Math.abs;

public class TaskCh11N245 {

    public static void main(String[] args) {
        System.out.println("Enter number elements of your array");
        Scanner inputNumber = new Scanner(System.in);
        int amountOfNumbers = inputNumber.nextInt();
        int[] startNumbers = fillTheArray(amountOfNumbers);
        int[] result = sortNumbers(startNumbers);
        for (int n = 0; n < result.length; n++) {
            System.out.print(result[n] + " ");
        }
    }

    public static int[] fillTheArray(int amountOfNumbers) {
        int[] emptyArray = new int[amountOfNumbers];
        System.out.println("Let's fill the array");
        Scanner inputVal = new Scanner(System.in);
        for (int i = 0; i < emptyArray.length; i++) {
            System.out.println("Value of your " + i + " element in array is");
            int number = inputVal.nextInt();
            emptyArray[i] = number;
        }
        return emptyArray;
    }

    public static int[] sortNumbers(int[] startNumbers) {
        int[] returnedNumbers = new int[startNumbers.length];
        for (int i = 0; i < startNumbers.length; i++) {
            if (startNumbers[i] != abs(startNumbers[i])) {
                int temp = startNumbers[i];
                for (int j = 0; j < i; j++) {
                    returnedNumbers[i - j] = returnedNumbers[i - j - 1];
                }
                returnedNumbers[0] = temp;
            } else {
                returnedNumbers[i] = startNumbers[i];
            }
        }
        return returnedNumbers;
    }
}