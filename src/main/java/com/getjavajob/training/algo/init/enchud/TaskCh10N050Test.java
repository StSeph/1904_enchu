package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N050.countAckermannFunction;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {

    public static void main(String[] args) {
        testAckermannFunction();
    }

    public static void testAckermannFunction() {
        assertEquals("TaskCh10N050Test.testAckermannFunction", 5, countAckermannFunction(1, 3));
    }
}