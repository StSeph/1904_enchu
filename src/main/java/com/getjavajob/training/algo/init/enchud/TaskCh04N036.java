package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh04N036 {

    public static void main(String[] args) {
        System.out.println("How much minutes is passed after hour?");
        Scanner inputNumber = new Scanner(System.in);
        int minutes = inputNumber.nextInt();
        boolean greenOrRed = checkColor(minutes);
        System.out.println(greenOrRed ? "green" : "red");
    }

    public static boolean checkColor(int minutes) {
        return minutes % 5 < 3;
    }
}