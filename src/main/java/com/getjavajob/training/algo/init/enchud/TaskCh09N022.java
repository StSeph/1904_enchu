package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N022 {

    public static void main(String[] args) {
        System.out.println("Write the word what we will  \"cut\"");
        Scanner inputLine = new Scanner(System.in);
        String enteredWord = inputLine.nextLine();
        String halfOfWord = cutWord(enteredWord);
        System.out.println(halfOfWord);
    }

    public static String cutWord(String enteredWord) {
        if (enteredWord.length() % 2 == 0 && !enteredWord.isEmpty()) {
            return enteredWord.substring(0, enteredWord.length() / 2);
        } else {
            return "Hey! You entered empty line or length of entered word not even!";
        }
    }
}