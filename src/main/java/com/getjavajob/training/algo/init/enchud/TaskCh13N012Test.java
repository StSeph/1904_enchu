package com.getjavajob.training.algo.init.enchud;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh13N012Test {

    public static void main(String[] args) {
        List<Employee> testEmployees = new ArrayList<>();
        testEmployees.add(new Employee("Denis", "Suslov", "Vladimirovich", "Moskva", 11, 2018));
        testEmployees.add(new Employee("Anna", "Jyrova", "Denisovna", "Moskva", 1, 2012));
        testEmployees.add(new Employee("Vadim", "Koletov", "SPB", 5, 2013));
        testEmployees.add(new Employee("Kim", "Ir", "Sen", "Phenyan", 2, 2002));
        Database testDb = new Database(testEmployees);
        testSearchEmployee(testDb);
        System.out.println();
        testCountTimeOfWork(testDb);
    }

    public static void testSearchEmployee(Database testDb) {
        List<Employee> testResultEmployees = new ArrayList<>();
        testResultEmployees.add(new Employee("Denis", "Suslov", "Vladimirovich", "Moskva", 11, 2018));
        testResultEmployees.add(new Employee("Anna", "Jyrova", "Denisovna", "Moskva", 1, 2012));
        assertEquals("TaskCh13N012Test.testSearchEmployee", testResultEmployees, testDb.searchEmployee("Denis"));
    }

    public static void testCountTimeOfWork(Database testDb) {
        List<Employee> testResultTimeOfWork = new ArrayList<>();
        testResultTimeOfWork.add(new Employee("Kim", "Ir", "Sen", "Phenyan", 2, 2002));
        assertEquals("TaskCh13N012Test.testCountTimeOfWork", testResultTimeOfWork, testDb.countTimeOfWork(4, 2019, 10));
    }
}