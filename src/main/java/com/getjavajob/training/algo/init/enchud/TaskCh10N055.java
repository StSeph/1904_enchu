package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N055 {

    public static void main(String[] args) {
        System.out.println("Enter thr number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        System.out.println("Enter the number system");
        int numberSystem = inputNumber.nextInt();
        String stringNumber = convertNumber(number, numberSystem);
        System.out.println(stringNumber);
    }

    public static String convertNumber(int number, int numberSystem) {
        char[] numbersInArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        return number == 0 ? "" : convertNumber(number / numberSystem, numberSystem) + numbersInArray[number % numberSystem];
    }
}