package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh06N008.countPowNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {

    public static void main(String[] args) {
        testFirstCountPowNumbers();
        testSecondCountPowNumbers();
    }

    public static void testFirstCountPowNumbers() {
        int[] firstTestArray = {1, 4, 9, 16, 25, 36};
        assertEquals("TaskCh06N008Test.testFirstCountPowNumbers", firstTestArray, countPowNumbers(43));
    }

    public static void testSecondCountPowNumbers() {
        int[] secondTestArray = {1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121};
        assertEquals("TaskCh06N008Test.testSecondCountPowNumbers", secondTestArray, countPowNumbers(134));
    }
}