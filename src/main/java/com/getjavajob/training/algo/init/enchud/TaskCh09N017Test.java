package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N017.isSame;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N017Test {

    public static void main(String[] args) {
        testCheckFirstLetterSameLast();
    }

    public static void testCheckFirstLetterSameLast() {
        assertEquals("TaskCh09N017Test.testCheckFirstLetterSameLast", true, isSame("example"));
        assertEquals("TaskCh09N017Test.testCheckFirstLetterSameLast", true, isSame("notification"));
        assertEquals("TaskCh09N017Test.testCheckFirstLetterSameLast", false, isSame("drugs"));
    }
}