package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh04N015.countAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {

    public static void main(String[] args) {
        testFirstCountYourAge();
        testSecondCountYourAge();
        testThirdCountYourAge();
    }

    private static void testThirdCountYourAge() {
        assertEquals("TaskCh04N015Test.testFirstCountYourAge", 29, countAge(6, 1985, 12, 2014));
    }

    private static void testSecondCountYourAge() {
        assertEquals("TaskCh04N015Test.testFirstCountYourAge", 28, countAge(6, 1985, 5, 2014));
    }

    public static void testFirstCountYourAge() {
        assertEquals("TaskCh04N015Test.testFirstCountYourAge", 29, countAge(6, 1985, 6, 2014));
    }
}