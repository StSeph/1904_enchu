package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh12N023 {

    public static void main(String[] args) {
        System.out.println("Select array size. NxN - where N is odd number ");
        Scanner inputNumber = new Scanner(System.in);
        int size = inputNumber.nextInt();
        if (size % 2 == 1) {
            int[][] numbersTaskA = fillArrayTaskA(size);
            printArray(numbersTaskA);
            System.out.println();

            int[][] numbersTaskB = fillArrayTaskB(size);
            printArray(numbersTaskB);
            System.out.println();

            int[][] numbersTaskC = fillArrayTaskC(size);
            printArray(numbersTaskC);
        } else {
            System.out.println("The size must be odd. Restart programme, please");
        }
    }

    public static int[][] fillArrayTaskA(int size) {
        int[][] taskA = new int[size][size];
        for (int i = 0; i < taskA.length; i++) {
            taskA[i][i] = 1;
            taskA[taskA.length - i - 1][i] = 1;
        }
        return taskA;
    }

    public static int[][] fillArrayTaskB(int size) {
        int[][] taskB = new int[size][size];
        for (int i = 0; i < taskB.length; i++) {
            taskB[i][i] = 1;
            taskB[taskB.length - i - 1][i] = 1;
            taskB[i][taskB.length / 2] = 1;
            taskB[taskB.length / 2][i] = 1;
        }
        return taskB;
    }

    public static int[][] fillArrayTaskC(int size) {
        int[][] taskC = new int[size][size];
        if (taskC.length % 2 != 0) {
            for (int i = 0; i < taskC.length; i++) {
                for (int j = 0; j + i - 1 < taskC.length / 2; j++) {
                    taskC[i][taskC.length / 2 + j] = 1;
                    taskC[i][taskC.length / 2 - j] = 1;
                    taskC[taskC.length - i - 1][taskC.length / 2 + j] = 1;
                    taskC[taskC.length - i - 1][taskC.length / 2 - j] = 1;
                }
            }
        }
        return taskC;
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}