package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh11N245.sortNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {

    public static void main(String[] args) {
        testFirstSortNumbers();
        testSecondSortNumbers();
    }

    public static void testSecondSortNumbers() {
        int[] testSecondNumbers = {-1, -2, -3, -4, -5, 6};
        int[] testSecondSortNumbers = {-5, -4, -3, -2, -1, 6};
        assertEquals("TaskCh11N245Test.testFirstSortNumbers", testSecondSortNumbers, sortNumbers(testSecondNumbers));
    }

    public static void testFirstSortNumbers() {
        int[] testFirstNumbers = {1, -2, 3, 4, -5, 6};
        int[] testFirstSortNumbers = {-5, -2, 1, 3, 4, 6};
        assertEquals("TaskCh11N245Test.testFirstSortNumbers", testFirstSortNumbers, sortNumbers(testFirstNumbers));
    }
}
