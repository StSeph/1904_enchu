package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh12N023.fillArrayTaskA;
import static com.getjavajob.training.algo.init.enchud.TaskCh12N023.fillArrayTaskB;
import static com.getjavajob.training.algo.init.enchud.TaskCh12N023.fillArrayTaskC;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {

    public static void main(String[] args) {
        testSortArrayA();
        testSortArrayB();
        testSortArrayC();
    }

    public static void testSortArrayC() {
        int[][] testTaskC = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        assertEquals("TaskCh12N023Test.testFirstSortNumbers", testTaskC, fillArrayTaskC(7));
    }

    public static void testSortArrayB() {
        int[][] testTaskB = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testFirstSortNumbers", testTaskB, fillArrayTaskB(7));
    }

    public static void testSortArrayA() {
        int[][] testTaskA = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testFirstSortNumbers", testTaskA, fillArrayTaskA(7));
    }
}