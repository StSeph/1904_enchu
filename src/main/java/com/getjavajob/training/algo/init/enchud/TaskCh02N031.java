package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh02N031 {

    private static int MIN_NUMBER = 100;
    private static int MAX_NUMBER = 999;

    public static void main(String[] args) {
        System.out.println("Enter a three-digit number");
        Scanner inputNumber = new Scanner(System.in);
        int n = inputNumber.nextInt();
        int x = findStartNumber(n);
        if (n >= MIN_NUMBER && n <= MAX_NUMBER) {
            System.out.println("Your start number x = " + x);
        } else {
            System.out.println("You entered a non-three-digit number");
        }
    }

    public static int findStartNumber(int n) {
        int a = n / 100;
        int b = (n - a * 100) / 10;
        int c = n % 10;
        return a * 100 + c * 10 + b;
    }
}