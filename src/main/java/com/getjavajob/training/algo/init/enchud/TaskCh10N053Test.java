package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N053.reverseNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {

    public static void main(String[] args) {
        testReverseNumbers();
    }

    public static void testReverseNumbers() {
        int[] testEmptyArray = new int[5];
        int[] testNumbers = {4, 5, 6, 7, 8};
        int[] testResult = {8, 7, 6, 5, 4};
        assertEquals("TaskCh10N053Test.testReverseNumbers", testResult, reverseNumbers(testNumbers, testEmptyArray, testEmptyArray.length));
    }
}