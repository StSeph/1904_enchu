package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N051 {

    public static void main(String[] args) {
        System.out.println("Enter your number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        countExampleA(number);
        countExampleB(number);
        countExampleC(number);
    }

    public static void countExampleA(int number) {
        if (number > 0) {
            System.out.println(number);
            countExampleA(number - 1);
        }
    }

    public static void countExampleB(int number) {
        if (number > 0) {
            countExampleB(number - 1);
            System.out.println(number);
        }
    }

    public static void countExampleC(int number) {
        if (number > 0) {
            System.out.println(number);
            countExampleC(number - 1);
            System.out.println(number);
        }
    }
}