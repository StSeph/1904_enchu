package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh11N158.cleanMatrixFromRepetition;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {

    public static void main(String[] args) {
        testCleanArrayFromRepetition();
    }

    public static void testCleanArrayFromRepetition() {
        String[] testWordsWithRepeaters = {"3", "e", "o", "slow", "9", "e", "slow", "1", "e43", "o", "e43"};
        String[] testCleanWords = {"3", "e", "o", "slow", "9", "1", "e43", "0", "0", "0", "0"};
        assertEquals("TaskCh11N158Test.testCleanArrayFromRepetition", testCleanWords, cleanMatrixFromRepetition(testWordsWithRepeaters));
    }
}