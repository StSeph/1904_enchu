package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh02N013 {

    private static final int MINIMUM_NUMBER = 100;
    private static final int MAXIMUM_NUMBER = 200;

    public static void main(String[] args) {
        System.out.println("Enter a three-digit number from 100 to 199");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        int result = reverseNumber(number);
        if (number > MINIMUM_NUMBER && number < MAXIMUM_NUMBER) {
            System.out.println("Your reversed number is " + result);
        } else {
            System.out.println("Your number is not within the specified range");
        }
    }

    public static int reverseNumber(int number) {
        int a = number / 100;
//        If yourNumber would be greater than 200, then the formula will be (yourNumber is a * 100) / 10
        int b = (number - 100) / 10;
        int c = number % 10;
        return c * 100 + b * 10 + a;
    }
}