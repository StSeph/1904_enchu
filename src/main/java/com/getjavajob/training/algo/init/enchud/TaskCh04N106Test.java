package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh04N106.determineSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {

    public static void main(String[] args) {
        testDetermineWinter();
        testDetermineSummer();
        testDetermineSpring();
        testDetermineAutumn();
    }

    private static void testDetermineWinter() {
        assertEquals("TaskCh04N106Test.testDetermineWinter", "winter", determineSeason(2));
    }

    private static void testDetermineSummer() {
        assertEquals("TaskCh04N106Test.testDetermineSummer", "summer", determineSeason(6));
    }

    private static void testDetermineSpring() {
        assertEquals("TaskCh04N106Test.testDetermineSpring", "spring", determineSeason(5));
    }

    public static void testDetermineAutumn() {
        assertEquals("TaskCh04N106Test.testDetermineAutumn", "autumn", determineSeason(11));
    }
}