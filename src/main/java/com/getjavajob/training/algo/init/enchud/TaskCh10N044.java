package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N044 {

    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        int digitalRoot = countRoot(number);
        System.out.println("Your digital root is " + digitalRoot);
    }

    public static int countRoot(int number) {
        if (number == 0) {
            return 0;
        }
        number = number % 10 + countRoot(number / 10);
        if (number > 9) {
            return number % 10 + countRoot(number / 10);
        } else {
            return number;
        }
    }
}