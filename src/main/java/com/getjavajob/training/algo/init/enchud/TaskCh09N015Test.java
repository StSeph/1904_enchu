package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N015.findLetter;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {

    public static void main(String[] args) {
        testFindLetter();
    }

    public static void testFindLetter() {
        assertEquals("TaskCh09N015Test.testFindLetter", 'e', findLetter("Hello", 2));
    }
}