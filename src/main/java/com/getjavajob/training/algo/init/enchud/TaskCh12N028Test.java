package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh12N028.fillArrayWithSnake;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N028Test {

    public static void main(String[] args) {
        testFillArrayWithSnake();
    }

    public static void testFillArrayWithSnake() {
        int[][] testMatrix = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        assertEquals("TaskCh12N028Test.testFirstSortNumbers", testMatrix, fillArrayWithSnake(5));
    }
}