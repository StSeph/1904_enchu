package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {

    public static void main(String[] args) {
        Game testFirsGame = new Game();
        testGame(testFirsGame);
        testScore(testFirsGame);
        testScoreDraw(testFirsGame);
    }

    public static void testGame(Game testFirsGame) {
        testFirsGame.setTeamOnePoint(20);
        testFirsGame.setTeamTwoPoint(17);
        testFirsGame.setTeamOneName("Lokomotiv");
        testFirsGame.setTeamTwoName("Spartak");
        assertEquals("TaskCh06N087Test.testGame", "Lokomotiv won! Spartak lost!", testFirsGame.result());
    }

    public static void testScore(Game testFirsGame) {
        testFirsGame.setTeamOnePoint(10);
        testFirsGame.setTeamTwoPoint(2);
        testFirsGame.setTeamOneName("Lokomotiv");
        testFirsGame.setTeamTwoName("Spartak");
        assertEquals("TaskCh06N087Test.testIntermediateScore", "Lokomotiv 10 - 2 Spartak", testFirsGame.score());
    }

    public static void testScoreDraw(Game testFirsGame) {
        testFirsGame.setTeamOnePoint(12);
        testFirsGame.setTeamTwoPoint(12);
        testFirsGame.setTeamOneName("Lokomotiv");
        testFirsGame.setTeamTwoName("Spartak");
        assertEquals("TaskCh06N087Test.testIntermediateScore", "Won is..... Friendship :)", testFirsGame.result());
    }

}