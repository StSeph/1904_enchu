package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N044.countRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {

    public static void main(String[] args) {
        testCountRoot();
    }

    public static void testCountRoot() {
        assertEquals("TaskCh10N044Test.testCountRoot", 6, countRoot(555));
    }
}