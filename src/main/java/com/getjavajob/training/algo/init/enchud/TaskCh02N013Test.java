package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh02N013.reverseNumber;
import static com.getjavajob.training.algo.util.Assert.*;

public class TaskCh02N013Test {

    public static void main(String[] args) {
        testReplaceNumber();
    }

    public static void testReplaceNumber() {
        assertEquals("TaskCh02N013Test.testReplaceNumber", 321, reverseNumber(123));
    }
}