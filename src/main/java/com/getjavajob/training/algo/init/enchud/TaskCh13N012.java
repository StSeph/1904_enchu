package com.getjavajob.training.algo.init.enchud;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class TaskCh13N012 {

    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Eugene", "Ivanov", "Sergeevich", "Kaliningrad", 11, 2004));
        employees.add(new Employee("Andrei", "Karpov", "Viktorovich", "Gusev", 4, 2008));
        employees.add(new Employee("Dennis", "Smith", "Gurevsk", 3, 2012));
        employees.add(new Employee("Kirill", "Bantikov", "Yurevich", "Moscow", 8, 2011));
        employees.add(new Employee("Vladimir", "Bojenko", "Leonidovich", "Minsk", 7, 2016));
        employees.add(new Employee("Timur", "Gaifullin", "Kirillovich", "Kazan", 4, 2010));
        Database taskDatabase = new Database(employees);
        System.out.println("Please, choose what you want: ");
        System.out.println("1. Search employee have keyword.");
        System.out.println("2. Search employee for time work.");
        Scanner inputNumber = new Scanner(System.in);
        int choice = inputNumber.nextInt();
        switch (choice) {
            case 1:
                List<Employee> resultEmployeeSearch = taskDatabase.searchEmployee("Kirill");
                for (int i = 0; i < resultEmployeeSearch.size(); i++) {
                    System.out.println(resultEmployeeSearch.get(i));
                }
                break;
            case 2:
                Scanner inputNumber = new Scanner(System.in);
                int n
                List<Employee> timeWorkResult = taskDatabase.countTimeOfWork(4, 2019, 10);
                for (int j = 0; j < timeWorkResult.size(); j++) {
                    System.out.println(timeWorkResult.get(j));
                }
                break;
            default:
                System.out.println("You entered wrong command. Restart programme, please.");
        }
    }
}

class Employee {
    private String name;
    private String surname;
    private String patronymic;
    private String address;
    private int monthStartWorking;
    private int yearStartWorking;

    public Employee(String name, String surname, String address, int monthStartWorking, int yearStartWorking) {
        this(name, surname, "", address, monthStartWorking, yearStartWorking);
    }

    public Employee(String name, String surname, String patronymic, String address, int monthStartWorking, int yearStartWorking) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.address = address;
        this.monthStartWorking = monthStartWorking;
        this.yearStartWorking = yearStartWorking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return monthStartWorking == employee.monthStartWorking &&
                yearStartWorking == employee.yearStartWorking &&
                name.equals(employee.name) &&
                surname.equals(employee.surname) &&
                Objects.equals(patronymic, employee.patronymic) &&
                address.equals(employee.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, address, monthStartWorking, yearStartWorking);
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + patronymic + " " + address + " " + monthStartWorking + " " + yearStartWorking;
    }

    public int getYearStartWorking() {
        return yearStartWorking;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int countExperience(int currentMonth, int currentYear) {
        int result = currentYear - getYearStartWorking();
        return currentMonth <= getYearStartWorking() ? result + 1 : result;
    }
}

class Database {
    private List<Employee> employees;

    public Database(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> searchEmployee(String searchWord) {
        List<Employee> resultEmployee = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getName().toLowerCase().contains(searchWord.toLowerCase())
                    || employee.getSurname().toLowerCase().contains(searchWord.toLowerCase())
                    || employee.getPatronymic().toLowerCase().contains(searchWord.toLowerCase())) {
                resultEmployee.add(employee);
            }
        }
        return resultEmployee;
    }

    public List<Employee> countTimeOfWork(int month, int year, int neededExp) {
        List<Employee> timeWorkResult = new ArrayList<>();
        if (month > 0 && month < 13) {
            for (Employee employee : employees) {
                if (employee.countExperience(month, year) >= neededExp) {
                    timeWorkResult.add(employee);
                }
            }
        }
        return timeWorkResult;
    }
}