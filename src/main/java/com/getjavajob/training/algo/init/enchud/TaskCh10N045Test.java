package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N045.countStepNumber;
import static com.getjavajob.training.algo.init.enchud.TaskCh10N045.countSumOfNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {

    public static void main(String[] args) {
        testCountStepNumber();
        testCountSumOfNumbers();
    }

    public static void testCountStepNumber() {
        assertEquals("TaskCh10N045Test.testCountStepNumber", 19, countStepNumber(4, 3, 5));
    }

    public static void testCountSumOfNumbers() {
        assertEquals("TaskCh10N045Test.testCountSumOfNumbers", 70, countSumOfNumbers(2, 6, 4));
    }
}