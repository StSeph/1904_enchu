package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N022.cutWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {

    public static void main(String[] args) {
        testCutWord();
    }

    public static void testCutWord() {
        assertEquals("TaskCh09N022Test.testCutWord", "Goo", cutWord("Google"));
    }
}