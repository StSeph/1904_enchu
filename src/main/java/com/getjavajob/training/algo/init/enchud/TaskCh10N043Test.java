package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N043.countQuantityDigits;
import static com.getjavajob.training.algo.init.enchud.TaskCh10N043.countSumDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {

    public static void main(String[] args) {
        testCountSumDigits();
        testCountQuantityDigits();
    }

    public static void testCountSumDigits() {
        assertEquals("TaskCh10N043Test.testCountSumDigits", 15, countSumDigits(12345));
    }

    public static void testCountQuantityDigits() {
        assertEquals("TaskCh10N043Test.testCountQuantityDigits", 8, countQuantityDigits(90802030));
    }
}