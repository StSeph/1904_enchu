package com.getjavajob.training.algo.init.enchud;

public class TaskCh12N025 {

    public static void main(String[] args) {
        int[][] taskA = fillArrayTaskA();
        int[][] taskB = fillArrayTaskB();
        int[][] taskC = fillArrayTaskC();
        int[][] taskD = fillArrayTaskD();
        int[][] taskE = fillArrayTaskE();
        int[][] taskF = fillArrayTaskF();
        int[][] taskG = fillArrayTaskG();
        int[][] taskH = fillArrayTaskH();
        int[][] taskI = fillArrayTaskI();
        int[][] taskJ = fillArrayTaskJ();
        int[][] taskK = fillArrayTaskK();
        int[][] taskL = fillArrayTaskL();
        int[][] taskM = fillArrayTaskM();
        int[][] taskN = fillArrayTaskN();
        int[][] taskO = fillArrayTaskO();
        int[][] taskP = fillArrayTaskP();
        System.out.println("Task a");
        printArray(taskA);
        System.out.println();
        System.out.println("Task b");
        printArray(taskB);
        System.out.println();
        System.out.println("Task c");
        printArray(taskC);
        System.out.println();
        System.out.println("Task d");
        printArray(taskD);
        System.out.println();
        System.out.println("Task e");
        printArray(taskE);
        System.out.println();
        System.out.println("Task f");
        printArray(taskF);
        System.out.println();
        System.out.println("Task g");
        printArray(taskG);
        System.out.println();
        System.out.println("Task h");
        printArray(taskH);
        System.out.println();
        System.out.println("Task i");
        printArray(taskI);
        System.out.println();
        System.out.println("Task j");
        printArray(taskJ);
        System.out.println();
        System.out.println("Task k");
        printArray(taskK);
        System.out.println();
        System.out.println("Task L");
        printArray(taskL);
        System.out.println();
        System.out.println("Task m");
        printArray(taskM);
        System.out.println();
        System.out.println("Task n");
        printArray(taskN);
        System.out.println();
        System.out.println("Task o");
        printArray(taskO);
        System.out.println();
        System.out.println("Task p");
        printArray(taskP);
    }

    public static int[][] fillArrayTaskA() {
        int[][] firstArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < firstArray[0].length; j++, step++) {
                firstArray[i][j] = step;
            }
        }
        return firstArray;
    }

    public static int[][] fillArrayTaskB() {
        int[][] secondArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < secondArray[0].length; i++) {
            for (int j = 0; j < secondArray.length; j++, step++) {
                secondArray[j][i] = step;
            }
        }
        return secondArray;
    }

    public static int[][] fillArrayTaskC() {
        int[][] thirdArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < thirdArray.length; i++) {
            for (int j = 0; j < thirdArray[0].length; j++, step++) {
                thirdArray[i][thirdArray[0].length - j - 1] = step;
            }
        }
        return thirdArray;
    }

    public static int[][] fillArrayTaskD() {
        int[][] fourthArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < fourthArray[0].length; i++) {
            for (int j = fourthArray.length - 1; j >= 0; j--, step++) {
                fourthArray[j][i] = step;
            }
        }
        return fourthArray;
    }

    public static int[][] fillArrayTaskE() {
        int[][] fifthArray = new int[10][12];
        int step = 1;
        for (int i = 0; i < fifthArray.length; i++) {
            for (int j = 0; j < fifthArray[0].length; j++, step++) {
                fifthArray[i][j] = step;
            }
            i++;
            for (int k = fifthArray[0].length - 1; k >= 0; k--, step++) {
                fifthArray[i][k] = step;
            }
        }
        return fifthArray;
    }

    public static int[][] fillArrayTaskF() {
        int[][] sixthArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < sixthArray[0].length; i++) {
            for (int j = 0; j < sixthArray.length; j++, step++) {
                sixthArray[j][i] = step;
            }
            i++;
            for (int k = sixthArray.length - 1; k >= 0; k--, step++) {
                sixthArray[k][i] = step;
            }
        }
        return sixthArray;
    }

    public static int[][] fillArrayTaskG() {
        int[][] seventhArray = new int[12][10];
        int step = 1;
        for (int i = seventhArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < seventhArray[0].length; j++, step++) {
                seventhArray[i][j] = step;
            }
        }
        return seventhArray;
    }

    public static int[][] fillArrayTaskH() {
        int[][] eighthArray = new int[12][10];
        int step = 1;
        for (int i = eighthArray[0].length - 1; i >= 0; i--) {
            for (int j = 0; j < eighthArray.length; j++, step++) {
                eighthArray[j][i] = step;
            }
        }
        return eighthArray;
    }

    public static int[][] fillArrayTaskI() {
        int[][] ninthArray = new int[12][10];
        int step = 1;
        for (int i = ninthArray.length - 1; i >= 0; i--) {
            for (int j = ninthArray[0].length - 1; j >= 0; j--, step++) {
                ninthArray[i][j] = step;
            }
        }
        return ninthArray;
    }

    public static int[][] fillArrayTaskJ() {
        int[][] tenthArray = new int[12][10];
        int step = 1;
        for (int i = tenthArray[0].length - 1; i >= 0; i--) {
            for (int j = tenthArray.length - 1; j >= 0; j--, step++) {
                tenthArray[j][i] = step;
            }
        }
        return tenthArray;
    }

    public static int[][] fillArrayTaskK() {
        int[][] eleventhArray = new int[12][10];
        int step = 1;
        for (int i = eleventhArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < eleventhArray[0].length; j++, step++) {
                eleventhArray[i][j] = step;
            }
            i--;
            for (int k = eleventhArray[0].length - 1; k >= 0; k--, step++) {
                eleventhArray[i][k] = step;
            }
        }
        return eleventhArray;
    }

    public static int[][] fillArrayTaskL() {
        int[][] twelfthArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < twelfthArray.length; i++) {
            for (int j = twelfthArray[0].length - 1; j >= 0; j--, step++) {
                twelfthArray[i][j] = step;
            }
            i++;
            for (int k = 0; k < twelfthArray[0].length; k++, step++) {
                twelfthArray[i][k] = step;
            }
        }
        return twelfthArray;
    }

    public static int[][] fillArrayTaskM() {
        int[][] sixteenthArray = new int[12][10];
        int step = 1;
        for (int i = sixteenthArray[0].length - 1; i >= 0; i--) {
            for (int j = 0; j < sixteenthArray.length; j++, step++) {
                sixteenthArray[j][i] = step;
            }
            i--;
            for (int k = sixteenthArray.length - 1; k >= 0; k--, step++) {
                sixteenthArray[k][i] = step;
            }
        }
        return sixteenthArray;
    }

    public static int[][] fillArrayTaskN() {
        int[][] thirteenthArray = new int[12][10];
        int step = 1;
        for (int i = 0; i < thirteenthArray[0].length; i++) {
            for (int j = thirteenthArray.length - 1; j >= 0; j--, step++) {
                thirteenthArray[j][i] = step;
            }
            i++;
            for (int k = 0; k < thirteenthArray.length; k++, step++) {
                thirteenthArray[k][i] = step;
            }
        }
        return thirteenthArray;
    }

    public static int[][] fillArrayTaskO() {
        int[][] fourteenthArray = new int[12][10];
        int step = 1;
        for (int i = fourteenthArray.length - 1; i >= 0; i--) {
            for (int j = fourteenthArray[0].length - 1; j >= 0; j--, step++) {
                fourteenthArray[i][j] = step;
            }
            i--;
            for (int k = 0; k < fourteenthArray[0].length; k++, step++) {
                fourteenthArray[i][k] = step;
            }
        }
        return fourteenthArray;
    }

    public static int[][] fillArrayTaskP() {
        int[][] fifteenthArray = new int[12][10];
        int step = 1;
        for (int i = fifteenthArray[0].length - 1; i >= 0; i--) {
            for (int j = fifteenthArray.length - 1; j >= 0; j--, step++) {
                fifteenthArray[j][i] = step;
            }
            i--;
            for (int k = 0; k < fifteenthArray.length; k++, step++) {
                fifteenthArray[k][i] = step;
            }
        }
        return fifteenthArray;
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}