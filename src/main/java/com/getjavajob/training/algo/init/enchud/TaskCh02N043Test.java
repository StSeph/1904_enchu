package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh02N043.areDivided;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N043Test {

    public static void main(String[] args) {

        testAreDividedAB();
        testAreDividedBA();
        testNoDivided();
    }

    public static void testAreDividedAB() {
        assertEquals("TaskCh02N043.restAreDivideAB", true, areDivided(9, 3));
    }

    public static void testAreDividedBA() {
        assertEquals("TaskCh02N043.restAreDivideBA", true, areDivided(0, 12));
    }

    public static void testNoDivided() {
        assertEquals("TaskCh02N043.restNoDivide", false, areDivided(2, 21));
    }
}