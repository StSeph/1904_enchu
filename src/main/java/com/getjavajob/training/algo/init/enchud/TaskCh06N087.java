package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh06N087 {

    public static void main(String[] args) {
        Game firstGame = new Game();
        firstGame.play();
    }
}

class Game {

    private static int MIN_POINT = 1;
    private static int MAX_POINT = 3;

    private int teamOnePoint;
    private int teamTwoPoint;
    private String teamOneName;
    private String teamTwoName;

    public void setTeamOnePoint(int teamOnePoint) {
        this.teamOnePoint = teamOnePoint;
    }

    public void setTeamTwoPoint(int teamTwoPoint) {
        this.teamTwoPoint = teamTwoPoint;
    }

    public void setTeamOneName(String teamOneName) {
        this.teamOneName = teamOneName;
    }

    public void setTeamTwoName(String teamTwoName) {
        this.teamTwoName = teamTwoName;
    }

    public void play() {
        System.out.println("Enter team #1: ");
        Scanner enterTeamName = new Scanner(System.in);
        teamOneName = enterTeamName.nextLine();
        System.out.println("Enter team #2: ");
        teamTwoName = enterTeamName.nextLine();
        boolean continueGame = true;
        Scanner inputNumber = new Scanner(System.in);
        while (continueGame) {
            String result = result();
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int act = inputNumber.nextInt();
            switch (act) {
                case 1:
                    System.out.println("Enter score (1 or 2 or 3): ");
                    int addPointToFirstTeam = inputNumber.nextInt();
                    if (addPointToFirstTeam >= MIN_POINT && addPointToFirstTeam <= MAX_POINT) {
                        teamOnePoint += addPointToFirstTeam;
                    } else {
                        System.out.println("Wrong number of point. Repeat command, please");
                    }
                    String scoreOne = score();
                    System.out.println(scoreOne);
                    break;
                case 2:
                    System.out.println("Enter score (1 or 2 or 3):");
                    int addPointToSecondTeam = inputNumber.nextInt();
                    if (addPointToSecondTeam >= MIN_POINT && addPointToSecondTeam <= MAX_POINT) {
                        teamTwoPoint += addPointToSecondTeam;
                    } else {
                        System.out.println("Wrong number of point. Repeat command, please");
                    }
                    String scoreTwo = score();
                    System.out.println(scoreTwo);
                    break;
                case 0:
                    continueGame = false;
                    String scoreEnd = score();
                    System.out.println(scoreEnd);
                    System.out.println(result);
                    break;
                default:
                    System.out.println("Wrong command! Enter number from 0 to 2");
                    break;
            }
        }
    }

    public String score() {
        return teamOneName + " " + teamOnePoint + " - " + teamTwoPoint + " " + teamTwoName;
    }

    public String result() {
        if (teamOnePoint > teamTwoPoint) {
            return teamOneName + " won! " + teamTwoName + " lost!";
        } else if (teamOnePoint < teamTwoPoint) {
            return teamTwoName + " won! " + teamOneName + " lost!";
        } else {
            return "Won is..... Friendship :)";
        }
    }
}