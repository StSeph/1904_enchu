package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh04N115.determineColorIsNow;
import static com.getjavajob.training.algo.init.enchud.TaskCh04N115.determineWhatAnimalYear;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {

    public static void main(String[] args) {
        testDetermineAnimalLessStartPoint();
        testDetermineAnimalMoreStartPoint();
        testDetermineColorLessStartPoint();
        testDetermineColorMoreStartPoint();
    }

    private static void testDetermineColorMoreStartPoint() {
        assertEquals("TaskCh04N115Test.testDetermineColorIsNow", "Green", determineColorIsNow(2014));
    }

    private static void testDetermineColorLessStartPoint() {
        assertEquals("TaskCh04N115Test.testDetermineColorIsNow", "White", determineColorIsNow(1981));
    }

    private static void testDetermineAnimalMoreStartPoint() {
        assertEquals("TaskCh04N115Test.testDetermineAnimalIsNow", "Dragon", determineWhatAnimalYear(2012));
    }

    private static void testDetermineAnimalLessStartPoint() {
        assertEquals("TaskCh04N115Test.testDetermineAnimalIsNow", "Dog", determineWhatAnimalYear(1970));
    }
}