package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh03N029 {

    public static void main(String[] args) {
        Scanner inputNumber = new Scanner(System.in);
        System.out.println("Enter х variable");
        int x = inputNumber.nextInt();
        System.out.println("Enter y variable");
        int y = inputNumber.nextInt();
        System.out.println("Enter z variable");
        int z = inputNumber.nextInt();
        boolean firstExample = countFirstExample(x, y);
        System.out.println("Is X and Y even? - " + firstExample);

        boolean secondExample = countSecondExample(x, y);
        System.out.println("Only one of the numbers X and Y is less than 20? - " + secondExample);

        boolean thirdExample = countThirdExample(x, y);
        System.out.println("At least one of the numbers X and Y is zero? - " + thirdExample);

        boolean fourthExample = countFourthExample(x, y, z);
        System.out.println("Each of the numbers X, Y, Z is negative? - " + fourthExample);

        boolean fifthExample = countFifthExample(x, y, z);
        System.out.println("Only one of the numbers X, Y and Z is a multiple of five? - " + fifthExample);

        boolean sixthExample = countSixthExample(x, y, z);
        System.out.println("At least one of the numbers X, Y, Z is greater than 100? - " + sixthExample);
    }

    /**
     * Each of the numbers X and Y is odd
     *
     * @param x first number
     * @param y second number
     */
    public static boolean countFirstExample(int x, int y) {
        return x % 2 == 1 && y % 2 == 1;
    }

    /**
     * Only one of the numbers X and Y is less than 20
     *
     * @param x first number
     * @param y second number
     */
    public static boolean countSecondExample(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * At least one of the numbers X and Y is zero
     *
     * @param x first number
     * @param y second number
     */
    public static boolean countThirdExample(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * Each of the numbers X, Y, Z is negative
     *
     * @param x first number
     * @param y second number
     * @param z third number
     */
    public static boolean countFourthExample(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * Only one of the numbers X, Y, and Z is a multiple of five
     *
     * @param x first number
     * @param y second number
     * @param z third number
     */
    public static boolean countFifthExample(int x, int y, int z) {
        boolean a = x % 5 == 0;
        boolean b = y % 5 == 0;
        boolean c = z % 5 == 0;
        return a ^ b ^ c && !(a && b && c);
    }

    /**
     * At least one of the numbers X, Y, Z is greater than 100
     *
     * @param x first number
     * @param y second number
     * @param z third number
     */
    public static boolean countSixthExample(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}