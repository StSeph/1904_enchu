package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh02N039.countDegrees;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {

    public static void main(String[] args) {
        testCountDegrees();
    }

    public static void testCountDegrees() {
        assertEquals("TaskCh02N031Test.testCountDegrees", 45.25, countDegrees(13, 30, 30));
    }
}