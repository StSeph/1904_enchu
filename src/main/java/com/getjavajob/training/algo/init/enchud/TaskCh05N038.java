package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh05N038 {

    public static void main(String[] args) {
        System.out.println("Enter number of stage");
        Scanner inputNumber = new Scanner(System.in);
        double stage = inputNumber.nextInt();
        double distanceFromHome = countDistance(stage);
        double allDistance = countAllDistance(stage);
        System.out.println("Husband is " + distanceFromHome + " kilometers from home now");
        System.out.println("Husband walked " + allDistance + " kilometres");
    }

    public static double countDistance(double stage) {
        double wayFromHome = 0;
        for (double i = 1; i <= stage; i++) {
            if (i % 2 == 1) {
                wayFromHome += 1 / i;
            } else {
                wayFromHome -= 1 / i;
            }
        }
        return wayFromHome;
    }

    public static double countAllDistance(double stage) {
        double allWay = 0;
        for (double j = 1; j <= stage; j++) {
            allWay += 1 / j;
        }
        return allWay;
    }
}