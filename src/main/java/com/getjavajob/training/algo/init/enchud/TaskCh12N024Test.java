package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh12N024.fillArrayTaskA;
import static com.getjavajob.training.algo.init.enchud.TaskCh12N024.fillArrayTaskB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {

    public static void main(String[] args) {
        testFillArrayTaskA();
        testFillArrayTaskB();
    }

    public static void testFillArrayTaskB() {
        int[][] testTaskB = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.testFillArrayTaskA", testTaskB, fillArrayTaskB(6));
    }

    public static void testFillArrayTaskA() {
        int[][] testTaskA = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024Test.testFillArrayTaskA", testTaskA, fillArrayTaskA(6));
    }
}