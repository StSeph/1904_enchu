package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh05N010 {

    private static int AMOUNT_OF_DOLLARS = 20;

    public static void main(String[] args) {
        System.out.println("Enter course of dollar");
        Scanner inputNumber = new Scanner(System.in);
        double course = inputNumber.nextDouble();
        double[] amountOfRubles = exchangeDollars(course);
        for (int dollars = 0; dollars < AMOUNT_OF_DOLLARS; dollars++)
            System.out.println((dollars + 1) + " - " + amountOfRubles[dollars]);
    }

    public static double[] exchangeDollars(double course) {
        double[] rubles = new double[AMOUNT_OF_DOLLARS];
        for (int i = 0; i < AMOUNT_OF_DOLLARS; i++) {
            rubles[i] = course * (i + 1);
        }
        return rubles;
    }
}