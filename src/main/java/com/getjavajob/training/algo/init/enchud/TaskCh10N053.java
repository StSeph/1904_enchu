package com.getjavajob.training.algo.init.enchud;

public class TaskCh10N053 {

    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};
        int[] emptyArray = new int[numbers.length];
        int step = numbers.length;
        int[] reversedNumber = reverseNumbers(numbers, emptyArray, step);
        for (int i = 0; i < reversedNumber.length; i++) {
            System.out.print(reversedNumber[i]);
        }
    }

    public static int[] reverseNumbers(int[] numbers, int[] emptyArray, int step) {
        if (step == 0) {
            return emptyArray;
        }
        emptyArray[step - 1] = numbers[numbers.length - step];
        reverseNumbers(numbers, emptyArray, step - 1);
        return emptyArray;
    }
}