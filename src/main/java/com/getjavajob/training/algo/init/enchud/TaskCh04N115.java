package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh04N115 {

    private static int START_POINT_YEAR = 1984;
    private static int NUMBER_OF_ANIMALS_PERIOD = 12;
    private static int NUMBER_OF_COLLOR_PERIOD = 10;

    public static void main(String[] args) {
        System.out.println("Enter the year from 1984");
        Scanner inputNumber = new Scanner(System.in);
        int n = inputNumber.nextInt();
        String eastYear = determineWhatAnimalYear(n) + ", " + determineColorIsNow(n);
        if (n > 0) {
            System.out.println(eastYear);
        } else {
            System.out.println("You entered wrong diapason year");
        }
    }

    public static String determineWhatAnimalYear(int n) {
        int animalsPeriod;
        if (n >= START_POINT_YEAR) {
            animalsPeriod = (n - START_POINT_YEAR) % NUMBER_OF_ANIMALS_PERIOD;
        } else {
            animalsPeriod = NUMBER_OF_ANIMALS_PERIOD - (START_POINT_YEAR - n) % NUMBER_OF_ANIMALS_PERIOD;
        }
        String animal = "";
        switch (animalsPeriod) {
            case 0:
                animal = "Rat";
                break;
            case 1:
                animal = "Cow";
                break;
            case 2:
                animal = "Tiger";
                break;
            case 3:
                animal = "Bunny";
                break;
            case 4:
                animal = "Dragon";
                break;
            case 5:
                animal = "Snake";
                break;
            case 6:
                animal = "Horse";
                break;
            case 7:
                animal = "Sheep";
                break;
            case 8:
                animal = "Monkey";
                break;
            case 9:
                animal = "Cock";
                break;
            case 10:
                animal = "Dog";
                break;
            case 11:
                animal = "Pig";
                break;
            default:
                break;
        }
        return animal;
    }

    public static String determineColorIsNow(int n) {
        int colorPeriod;
        if (n >= START_POINT_YEAR) {
            colorPeriod = (n - START_POINT_YEAR) % NUMBER_OF_COLLOR_PERIOD;
        } else {
            colorPeriod = NUMBER_OF_COLLOR_PERIOD - (START_POINT_YEAR - n) % NUMBER_OF_COLLOR_PERIOD;
        }
        String color = "";
        switch (colorPeriod) {
            case 0:
            case 1:
                color = "Green";
                break;
            case 2:
            case 3:
                color = "Rad";
                break;
            case 4:
            case 5:
                color = "Yellow";
                break;
            case 6:
            case 7:
                color = "White";
                break;
            case 8:
            case 9:
                color = "Black";
                break;
            default:
                break;
        }
        return color;
    }
}