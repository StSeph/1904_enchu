package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh12N063 {

    private static int YEAR_CLASS = 11;
    private static int CLASS_PARALLELS = 4;

    public static void main(String[] args) {
        int[][] emptyPupilsScroll = new int[YEAR_CLASS][CLASS_PARALLELS];
        int[][] pupilsInSchool = countPupilsInEachClass(emptyPupilsScroll);
        for (int i = 0; i < YEAR_CLASS; i++) {
            System.out.println(i + " class average pupils is " + countAverageInYearClass(pupilsInSchool, i));
        }
    }

    public static int[][] countPupilsInEachClass(int[][] emptyPupilsScroll) {
        Scanner inputNumber = new Scanner(System.in);
        for (int i = 0; i < YEAR_CLASS; i++) {
            System.out.println("Enter pupils in four parallel " + (i + 1) + " classes");
            for (int j = 0; j < CLASS_PARALLELS; j++) {
                int pupilsInOneClass = inputNumber.nextInt();
                emptyPupilsScroll[i][j] = pupilsInOneClass;
            }
        }
        return emptyPupilsScroll;
    }

    public static int countAverageInYearClass(int[][] pupilsInSchool, int classYear) {
        int averageNumber = 0;
        for (int i = 0; i < pupilsInSchool[0].length; i++) {
            averageNumber += pupilsInSchool[classYear][i];
        }
        return averageNumber / 4;
    }
}