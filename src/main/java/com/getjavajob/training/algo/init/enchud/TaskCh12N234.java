package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh12N234 {

    public static void main(String[] args) {
        System.out.println("Enter the number of lines in your array");
        Scanner inputNumber = new Scanner(System.in);
        int matrixLines = inputNumber.nextInt();
        System.out.println("Enter the number of columns in your array");
        int matrixColumns = inputNumber.nextInt();
        int[][] emptyArray = new int[matrixLines][matrixColumns];
        int[][] numbers = fillTheArray(emptyArray);
        System.out.println("Chose what do you want to delete in your array");
        System.out.println("1. Delete line");
        System.out.println("2. Delete column");
        int choice = inputNumber.nextInt();
        switch (choice) {
            case 1:
                System.out.println("Enter number of line what you want to delete");
                int numberLine = inputNumber.nextInt();
                int[][] cutLine = cutLineFromNumbers(numbers, numberLine);
                printArray(cutLine);
                break;
            case 2:
                System.out.println("Enter number of column what you want to delete");
                int numberColumn = inputNumber.nextInt();
                int[][] cutColumn = cutColumnFromNumbers(numbers, numberColumn);
                printArray(cutColumn);
                break;
            default:
                break;
        }
    }

    public static int[][] fillTheArray(int[][] emptyArray) {
        System.out.println("Let's fill the array");
        Scanner inputVal = new Scanner(System.in);
        for (int i = 0; i < emptyArray.length; i++) {
            System.out.println("Fill the " + (i + 1) + " line");
            for (int j = 0; j < emptyArray[0].length; j++) {
                System.out.println("Enter value of " + (j + 1) + " element");
                int value = inputVal.nextInt();
                emptyArray[i][j] = value;
            }
        }
        return emptyArray;
    }

    public static int[][] cutLineFromNumbers(int[][] numbers, int numberLine) {
        if (numberLine != numbers.length - 1) {
            for (int i = 0; i < numbers[0].length; i++) {
                for (int j = numberLine; j + 1 < numbers.length; j++) {
                    numbers[j][i] = numbers[j + 1][i];
                }
                numbers[numbers.length - 1][i] = 0;
            }
        } else if (numberLine == numbers.length - 1) {
            for (int i = 0; i < numbers[0].length; i++) {
                numbers[numbers.length - 1][i] = 0;
            }
        }
        return numbers;
    }

    public static int[][] cutColumnFromNumbers(int[][] numbers, int numberColumn) {
        if (numberColumn != numbers[0].length - 1) {
            for (int i = 0; i < numbers.length; i++) {
                for (int j = numberColumn; j + 1 < numbers[0].length; j++) {
                    numbers[i][j] = numbers[i][j + 1];
                }
                numbers[i][numbers[0].length - 1] = 0;
            }
        } else if (numberColumn == numbers[0].length - 1) {
            for (int i = 0; i < numbers.length; i++) {
                numbers[i][numbers[0].length - 1] = 0;
            }
        }
        return numbers;
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}