package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N050 {

    public static void main(String[] args) {
        Scanner inputNumber = new Scanner(System.in);
        System.out.println("Enter n for the Ackermann  function");
        int n = inputNumber.nextInt();
        System.out.println("Now enter the value of m");
        int m = inputNumber.nextInt();
        if (n >= 0 && m >= 0) {
            int result = countAckermannFunction(n, m);
            System.out.println("Your result is  " + result);
        } else {
            System.out.println("n-number and m-number must be more than zero. Reload programme");
        }
    }

    public static int countAckermannFunction(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return countAckermannFunction(n - 1, 1);
        } else {
            return countAckermannFunction(n - 1, countAckermannFunction(n, m - 1));
        }
    }
}