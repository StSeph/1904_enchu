package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N107.changeLettersInWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {

    public static void main(String[] args) {
        testChangeLettersInWord();
    }

    public static void testChangeLettersInWord() {
        assertEquals("TaskCh09N107Test.testChangeLettersInWord", "glodiatar", changeLettersInWord("gladiator"));
    }
}