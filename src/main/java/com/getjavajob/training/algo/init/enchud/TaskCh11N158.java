package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh11N158 {

    public static void main(String[] args) {
        System.out.println("Enter number elements of your array");
        Scanner inputNumber = new Scanner(System.in);
        int amountOfNumbers = inputNumber.nextInt();
        String[] elements = fillTheArray(amountOfNumbers);
        String[] result = cleanMatrixFromRepetition(elements);
        System.out.println();
        for (int n = 0; n < result.length; n++) {
            System.out.print(result[n] + " ");
        }
    }

    public static String[] fillTheArray(int amountOfNumbers) {
        String[] emptyArray = new String[amountOfNumbers];
        System.out.println("Let's fill the array");
        Scanner word = new Scanner(System.in);
        for (int i = 0; i < emptyArray.length; i++) {
            System.out.println("Value of your " + i + " element in array is");
            String stringNumber = word.nextLine();
            emptyArray[i] = stringNumber;
        }
        for (int i = 0; i < emptyArray.length; i++) {
            System.out.print(emptyArray[i]);
        }
        return emptyArray;
    }

    public static String[] cleanMatrixFromRepetition(String[] words) {
        String[] tempArray = words;
        int times = 0;
        for (int j = 0; j < tempArray.length - times; j++) {
            for (int k = 1 + j; k < tempArray.length - times; ) {
                if (tempArray[j].equals(tempArray[k])) {
                    for (int m = 0; m < tempArray.length - k - 1 - times; m++) {
                        tempArray[k + m] = tempArray[k + m + 1];
                    }
                    tempArray[tempArray.length - 1 - times] = "0";
                    times++;
                } else k++;
            }
        }
        return tempArray;
    }
}