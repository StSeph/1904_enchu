package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh05N038.countAllDistance;
import static com.getjavajob.training.algo.init.enchud.TaskCh05N038.countDistance;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N038Test {

    public static void main(String[] args) {
        testCountDistance();
        testCountAllDistance();
    }

    public static void testCountDistance() {
        assertEquals("TaskCh05N038Test.testCountDistance", 0.688172179310195, countDistance(100));
    }

    public static void testCountAllDistance() {
        assertEquals("TaskCh05N038Test.testCountAllDistance", 5.187377517639621, countAllDistance(100));
    }
}