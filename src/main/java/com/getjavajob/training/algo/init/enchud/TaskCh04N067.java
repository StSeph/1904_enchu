package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh04N067 {

    public static void main(String[] args) {
        System.out.println("What is day of year now?");
        Scanner inputNumber = new Scanner(System.in);
        int k = inputNumber.nextInt();
        String result = checkWorkdayOrNot(k);
        System.out.println(result);
    }

    public static String checkWorkdayOrNot(int k) {
        if (k >= 1 && k <= 365) {
            return (k % 7 == 6 || k % 7 == 0) ? "Weekend" : "Workday";
        } else {
            return "You entered wrong number of day";
        }
    }
}