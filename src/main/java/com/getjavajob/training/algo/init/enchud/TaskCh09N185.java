package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N185 {

    public static void main(String[] args) {
        Scanner inputString = new Scanner(System.in);
        String formula = inputString.nextLine();
        String result = checkBracesInFormula(formula);
        System.out.println(result);
    }

    public static String checkBracesInFormula(String formula) {
        int leftBrace = 0;
        int rightBrace = 0;
        for (int i = 0; i < formula.length(); i++) {
            if (formula.charAt(i) == 40) {
                leftBrace++;
            }
            if (formula.charAt(i) == 41) {
                rightBrace++;
            }
        }
        if (leftBrace == rightBrace) {
            return "There is all correct!";
        } else if (leftBrace > rightBrace) {
            return "You have extra brace(s). There is(are) - " + (leftBrace - rightBrace);
        } else {
            return "First extra right bracket on position " + (formula.lastIndexOf(")") - (rightBrace - leftBrace - 1));
        }
    }
}