package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh03N029.countFirstExample;
import static com.getjavajob.training.algo.init.enchud.TaskCh03N029.countSecondExample;
import static com.getjavajob.training.algo.init.enchud.TaskCh03N029.countThirdExample;
import static com.getjavajob.training.algo.init.enchud.TaskCh03N029.countFourthExample;
import static com.getjavajob.training.algo.init.enchud.TaskCh03N029.countFifthExample;
import static com.getjavajob.training.algo.init.enchud.TaskCh03N029.countSixthExample;
import static com.getjavajob.training.algo.util.Assert.assertEquals;


public class TaskCh03N029Test {

    public static void main(String[] args) {
        testOddBoth();
        testEvenBoth();
        testOneEven();
        testOneOdd();

        testBothLess20();
        testBothMore20();
        testOneLess20();
        testOneMore20();

        testBothZero();
        testOneZero();
        testBothNotZero();
        testSecondZero();

        testThreeMoreZero();
        testNegativeX();
        testNegativeY();
        testNegativeZ();
        testPositiveX();
        testPositiveY();
        testPositiveZ();
        testThreeNegative();

        testAllMultiple5();
        testXMultiple5();
        testYMultiple5();
        testZMultiple5();
        testXNotMultiple5();
        testYNotMultiple5();
        testZNotMultiple5();
        testAllNotMultiple5();

        testAllMore100();
        testXMore100();
        testYMore100();
        testZMore100();
        testXLess100();
        testYLess100();
        testZLess100();
        testAllLess100();
    }

    private static void testOddBoth() {
        assertEquals("TaskCh03N029Test.testOddBoth", true, countFirstExample(11, 43));
    }

    private static void testEvenBoth() {
        assertEquals("TaskCh03N029Test.testEvenBoth", false, countFirstExample(10, 4));
    }

    private static void testOneEven() {
        assertEquals("TaskCh03N029Test.testOneEven", false, countFirstExample(10, 43));
    }

    private static void testOneOdd() {
        assertEquals("TaskCh03N029Test.testOneOdd", false, countFirstExample(3, 2));
    }

    private static void testBothLess20() {
        assertEquals("TaskCh03N029Test.testBothLess20", false, countSecondExample(3, 3));
    }

    private static void testBothMore20() {
        assertEquals("TaskCh03N029Test.testBothMore20", false, countSecondExample(243, 343));
    }

    private static void testOneLess20() {
        assertEquals("TaskCh03N029Test.testOneLess20", true, countSecondExample(243, 3));
    }

    private static void testOneMore20() {
        assertEquals("TaskCh03N029Test.testOneMore20", true, countSecondExample(2, 43));
    }

    private static void testBothZero() {
        assertEquals("TaskCh03N029Test.testBothZero", true, countThirdExample(0, 0));
    }

    private static void testOneZero() {
        assertEquals("TaskCh03N029Test.testOneZero", true, countThirdExample(0, 5));
    }

    private static void testBothNotZero() {
        assertEquals("TaskCh03N029Test.testBothNotZero", false, countThirdExample(4, 65));
    }

    private static void testSecondZero() {
        assertEquals("TaskCh03N029Test.testSecondZero", true, countThirdExample(4, 0));
    }

    private static void testThreeMoreZero() {
        assertEquals("TaskCh03N029Test.testThreeMoreZero", false, countFourthExample(2, 32, 35));
    }

    private static void testNegativeX() {
        assertEquals("TaskCh03N029Test.testNegativeX", false, countFourthExample(-2, 32, 5));
    }

    private static void testNegativeY() {
        assertEquals("TaskCh03N029Test.testNegativeY", false, countFourthExample(2, -32, 35));
    }

    private static void testNegativeZ() {
        assertEquals("TaskCh03N029Test.testNegativeZ", false, countFourthExample(2, 32, -35));
    }

    private static void testPositiveX() {
        assertEquals("TaskCh03N029Test.testPositiveX", false, countFourthExample(2, -32, -25));
    }

    private static void testPositiveY() {
        assertEquals("TaskCh03N029Test.testPositiveY", false, countFourthExample(-2, 2, -5));
    }

    private static void testPositiveZ() {
        assertEquals("TaskCh03N029Test.testPositiveZ", false, countFourthExample(-2, -32, 5));
    }

    private static void testThreeNegative() {
        assertEquals("TaskCh03N029Test.testThreeNegative", true, countFourthExample(-2, -32, -65));
    }

    private static void testAllMultiple5() {
        assertEquals("TaskCh03N029Test.testAllMultiple5", false, countFifthExample(75, 15, 155));
    }

    private static void testXMultiple5() {
        assertEquals("TaskCh03N029Test.testXMultiple5", true, countFifthExample(70, 13, 66));
    }

    private static void testYMultiple5() {
        assertEquals("TaskCh03N029Test.testYMultiple5", true, countFifthExample(7, 15, 1));
    }

    private static void testZMultiple5() {
        assertEquals("TaskCh03N029Test.testZMultiple5", true, countFifthExample(7, 13, 155));
    }

    private static void testXNotMultiple5() {
        assertEquals("TaskCh03N029Test.testXNotMultiple5", false, countFifthExample(42, 10, 190));
    }

    private static void testYNotMultiple5() {
        assertEquals("TaskCh03N029Test.testYNotMultiple5", false, countFifthExample(40, 1, 90));
    }

    private static void testZNotMultiple5() {
        assertEquals("TaskCh03N029Test.testZNotMultiple5", false, countFifthExample(40, 10, 98));
    }

    private static void testAllNotMultiple5() {
        assertEquals("TaskCh03N029Test.testAllNotMultiple5", false, countFifthExample(4, 12, 98));
    }

    private static void testAllMore100() {
        assertEquals("TaskCh03N029Test.testAllMore100", true, countSixthExample(123, 324, 244));
    }

    private static void testXMore100() {
        assertEquals("TaskCh03N029Test.testXMore100", true, countSixthExample(123, 24, 2));
    }

    private static void testYMore100() {
        assertEquals("TaskCh03N029Test.testYMore100", true, countSixthExample(13, 324, 2));
    }

    private static void testZMore100() {
        assertEquals("TaskCh03N029Test.testZMore100", true, countSixthExample(17, 4, 542));
    }

    private static void testXLess100() {
        assertEquals("TaskCh03N029Test.testXLess100", true, countSixthExample(3, 324, 542));
    }

    private static void testYLess100() {
        assertEquals("TaskCh03N029Test.testYLess100", true, countSixthExample(123, 3, 542));
    }

    private static void testZLess100() {
        assertEquals("TaskCh03N029Test.testZLess100", true, countSixthExample(123, 324, 4));
    }

    private static void testAllLess100() {
        assertEquals("TaskCh03N029Test.testAllLess100", false, countSixthExample(3, 4, 5));
    }

}