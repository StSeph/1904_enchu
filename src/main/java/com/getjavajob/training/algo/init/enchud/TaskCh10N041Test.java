package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N041.countFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N041Test {

    public static void main(String[] args) {
        testCountFactorial();
    }

    public static void testCountFactorial() {
        assertEquals("TaskCh10N041Test.testCountFactorial", 120, countFactorial(5));
    }
}