package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh04N015 {

    public static void main(String[] args) {
        Scanner inputDate = new Scanner(System.in);
        System.out.println("Enter the month of born");
        int monthOfBorn = inputDate.nextInt();
        System.out.println("Enter the year of born");
        int yearOfBorn = inputDate.nextInt();
        System.out.println("What month is now?");
        int monthNow = inputDate.nextInt();
        System.out.println("What year is now?");
        int yearNow = inputDate.nextInt();
        int age = countAge(monthOfBorn, yearOfBorn, monthNow, yearNow);
        System.out.println("Your age is " + age);
    }

    public static int countAge(int monthOfBorn, int yearOfBorn, int monthNow, int yearNow) {
        int result = yearNow - yearOfBorn;
        return monthNow >= monthOfBorn ? result : result - 1;
    }
}