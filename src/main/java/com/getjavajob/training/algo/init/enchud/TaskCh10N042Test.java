package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N042.countNumberInDegree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {

    public static void main(String[] args) {
        testCountNumberInDegree();
    }

    public static void testCountNumberInDegree() {
        assertEquals("TaskCh10N042Test.testCountNumberInDegree", 125, countNumberInDegree(5, 3));
    }
}