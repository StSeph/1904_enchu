package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N017 {

    public static void main(String[] args) {
        System.out.println("Write the word");
        Scanner inputLine = new Scanner(System.in);
        String enteredWord = inputLine.nextLine();
        boolean result = isSame(enteredWord);
        System.out.println("Does your word start and end the same letter?");
        System.out.println(result);
    }

    public static boolean isSame(String enteredWord) {
        return enteredWord.toLowerCase().charAt(0) == enteredWord.toLowerCase().charAt(enteredWord.length() - 1);
    }
}