package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh12N024 {

    public static void main(String[] args) {
        System.out.println("Select array size. Size must be even");
        Scanner inputNumber = new Scanner(System.in);
        int size = inputNumber.nextInt();
        if (size % 2 == 0) {
            int[][] numbersTaskA = fillArrayTaskA(size);
            printArray(numbersTaskA);
            System.out.println();

            int[][] numbersTaskB = fillArrayTaskB(size);
            printArray(numbersTaskB);
        } else {
            System.out.println("The size must be even. Restart programme, please");
        }
    }

    public static int[][] fillArrayTaskA(int size) {
        int[][] taskA = new int[size][size];
        for (int i = 0; i < taskA.length; i++) {
            taskA[0][i] = 1;
            taskA[i][0] = 1;
        }
        for (int i = 1; i < taskA.length; i++) {
            for (int j = 1; j < taskA.length; j++) {
                taskA[i][j] = taskA[i][j - 1] + taskA[i - 1][j];
            }
        }
        return taskA;
    }

    public static int[][] fillArrayTaskB(int size) {
        int[][] taskB = new int[size][size];
        int step = 0;
        for (int k = 0; k < taskB.length; k++) {
            for (int j = 0; j < taskB.length; j++) {
                taskB[k][j] = (j + step) % size + 1;
            }
            step++;
        }
        return taskB;
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}