package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N166.replaceFirstAndLastWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N166Test {

    public static void main(String[] args) {
        testChangeFirstAndLastWord();
    }

    public static void testChangeFirstAndLastWord() {
        String testWord = "I welcome everyone";
        assertEquals("TaskCh09N166Test.testChangeFirstAndLastWord", testWord, replaceFirstAndLastWord("everyone welcome I"));
    }
}