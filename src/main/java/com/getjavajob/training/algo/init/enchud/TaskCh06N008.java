package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class TaskCh06N008 {

    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        int[] numberOfPeriod = countPowNumbers(number);
        for (int i : numberOfPeriod) {
            System.out.println(i);
        }
    }

    public static int[] countPowNumbers(int number) {
        int period = (int) sqrt(number);
        int[] numbersInPow = new int[period];
        for (int i = 1; i <= period; i++) {
            numbersInPow[i - 1] = (int) pow(i, 2);
        }
        return numbersInPow;
    }
}