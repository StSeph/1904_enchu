package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh12N234.cutColumnFromNumbers;
import static com.getjavajob.training.algo.init.enchud.TaskCh12N234.cutLineFromNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {

    public static void main(String[] args) {
        testCutLine();
        testCutColumn();
    }

    public static void testCutLine() {
        int[][] testFirstNumbers = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        int[][] testCutLineNumbers = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9},
                {0, 0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.testCutLine", testCutLineNumbers, cutLineFromNumbers(testFirstNumbers, 2));
    }

    public static void testCutColumn() {
        int[][] testSecondNumbers = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        int[][] testCutColumnNumbers = new int[][]{
                {1, 3, 4, 5, 0},
                {16, 18, 19, 6, 0},
                {15, 25, 20, 7, 0},
                {14, 22, 21, 8, 0},
                {13, 11, 10, 9, 0}
        };
        assertEquals("TaskCh12N234Test.testCutColumn", testCutColumnNumbers, cutColumnFromNumbers(testSecondNumbers, 1));
    }
}