package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N049.findIndexOfBiggestElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {

    public static void main(String[] args) {
        testFindIndexOfBiggestElement();
    }

    public static void testFindIndexOfBiggestElement() {
        int[] testArray = {32, 875, 3, 656, 43};
        assertEquals("TaskCh10N049Test.testFindIndexOfBiggestElement", 1, findIndexOfBiggestElement(testArray, 875, 5));
    }
}