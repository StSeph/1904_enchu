package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N052.countDigitsInNumber;
import static com.getjavajob.training.algo.init.enchud.TaskCh10N052.reverseTheNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {

    public static void main(String[] args) {
        testFlipTheNumbers();
        testCountDigits();
    }

    public static void testCountDigits() {
        assertEquals("TaskCh10N052Test.testCountDigits", 3, countDigitsInNumber(432));
    }

    public static void testFlipTheNumbers() {
        int[] testEmptyArray = new int[3];
        int[] testNumbers = {3, 2, 1};
        assertEquals("TaskCh10N052Test.testFlipTheNumbers", testNumbers, reverseTheNumber(123, 3, testEmptyArray));
    }
}