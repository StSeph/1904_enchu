package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

public class TaskCh01N017 {

    public static void main(String[] args) {
        Scanner variables = new Scanner(System.in);
        System.out.println("Enter variable x");
        double x = variables.nextDouble();
        System.out.println("Enter variable a");
        double a = variables.nextDouble();
        System.out.println("Enter variable b");
        double b = variables.nextDouble();
        System.out.println("Enter variable c");
        double c = variables.nextDouble();
        double resultO = countFirstFormula(x);
        double resultP = countSecondFormula(x, a, b, c);
        double resultR = countThirdFormula(x);
        double resultS = countFourthFormula(x);
        System.out.println("Result of example №1 = " + resultO);

        System.out.println("Result of example №2 = " + resultP);

        System.out.println("Result of example №2 = " + resultR);

        System.out.println("Result of example №2 = " + resultS);
    }

    private static double countFirstFormula(double x) {
        return sqrt(1 - (pow(sin(x), 2)));
    }

    private static double countSecondFormula(double x, double a, double b, double c) {
        return 1 / (sqrt(a * pow(x, 2) + b * x + c));
    }

    private static double countThirdFormula(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    private static double countFourthFormula(double x) {
        return abs(x) + abs(x + 1);
    }
}