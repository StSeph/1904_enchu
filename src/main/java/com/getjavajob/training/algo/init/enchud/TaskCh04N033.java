package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh04N033 {

    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        boolean result = isOdd(number);
        System.out.println(result ? "The number you enter ends with an odd number" : "The number you enter ends with an even number");
    }

    public static boolean isOdd(int number) {
        return number % 2 == 1;
    }
}