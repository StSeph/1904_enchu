package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh02N039 {

    public static final int HOURS_ON_DIAL = 12;
    public static final int START_ON_DIAL = 0;
    public static final int END_HOUR_ON_DIAL = 23;
    public static final int END_MINUTE_SECOND_ON_DIAL = 59;
    public static final double DEGREES_IN_ONE_HOUR = 30;
    public static final double DEGREES_IN_ONE_MINUTE = 0.5;
    public static final double DEGREES_IN_ONE_SECOND = 0.00833333333333333;

    public static void main(String[] args) {
        System.out.println("Time format HH: MM: SS");
        System.out.println("How many hours is it now?");
        Scanner time = new Scanner(System.in);
        int h = time.nextInt();
        System.out.println("How many minutes is it now?");
        int m = time.nextInt();
        System.out.println("How many seconds is it now?");
        int s = time.nextInt();
        double yourResult = countDegrees(h, m, s);
        System.out.println("The hour hand has moved " + yourResult + " degrees!");
    }

    public static double countDegrees(int h, int m, int s) {
        double degree = 0;
        if (h >= START_ON_DIAL && h <= END_HOUR_ON_DIAL &&
                m >= START_ON_DIAL && m <= END_MINUTE_SECOND_ON_DIAL &&
                s >= START_ON_DIAL && s <= END_MINUTE_SECOND_ON_DIAL) {
            degree = h % HOURS_ON_DIAL * DEGREES_IN_ONE_HOUR +
                    m * DEGREES_IN_ONE_MINUTE +
                    s * DEGREES_IN_ONE_SECOND;
        }
        return degree;
    }
}