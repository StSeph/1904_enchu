package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh10N047 {

    public static void main(String[] args) {
        System.out.println("Enter Fibonacci Sequence member number");
        Scanner inputNumber = new Scanner(System.in);
        int step = inputNumber.nextInt();
        int result = countNumberFibonacci(step);
        System.out.println(result);
    }

    public static int countNumberFibonacci(int step) {
        if (step < 3) {
            return 1;
        }
        return countNumberFibonacci(step - 1) + countNumberFibonacci(step - 2);
    }
}