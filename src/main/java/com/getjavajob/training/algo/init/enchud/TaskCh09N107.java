package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N107 {

    public static void main(String[] args) {
        System.out.println("Write the word");
        Scanner inputLine = new Scanner(System.in);
        String enteredWord = inputLine.nextLine();
        String changedWord = changeLettersInWord(enteredWord);
        System.out.println(changedWord);
    }

    /**
     * This method change first letter "a" in word to "o", and last "o" to "a".
     */
    public static String changeLettersInWord(String enteredWord) {
        int firstLetterA = enteredWord.indexOf("a");
        int lastLetterO = enteredWord.lastIndexOf("o");
        if (firstLetterA >= 0 && lastLetterO >= 0) {
            StringBuilder wordForReplace = new StringBuilder(enteredWord);
            wordForReplace = wordForReplace.replace(firstLetterA, firstLetterA + 1, "o");
            wordForReplace = wordForReplace.replace(lastLetterO, lastLetterO + 1, "a");
            return wordForReplace.toString();
        } else {
            return "There aren't letter \"a\" and(or) letter \"o\"";
        }
    }
}