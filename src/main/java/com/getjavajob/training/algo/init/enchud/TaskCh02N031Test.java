package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh02N031.findStartNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N031Test {

    public static void main(String[] args) {
        testFoundStartNumber();
    }

    public static void testFoundStartNumber() {
        assertEquals("TaskCh02N031Test.testFoundStartNumber", 863, findStartNumber(836));
    }
}