package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

import static java.lang.Math.pow;

public class TaskCh10N052 {

    private static int DETERMINANT = 10;

    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        int amountOfDigitsInNumber = countDigitsInNumber(number);
        int[] numbersDigits = new int[amountOfDigitsInNumber];
        int[] result = reverseTheNumber(number, amountOfDigitsInNumber, numbersDigits);
        for (int i = 0; i < amountOfDigitsInNumber; i++) {
            System.out.print(result[i]);
        }
    }

    public static int countDigitsInNumber(int number) {
        int tempNumber = number;
        int tensOfNumber = 0;
        while (tempNumber > 0) {
            tempNumber /= DETERMINANT;
            tensOfNumber++;
        }
        return tensOfNumber;
    }

    public static int[] reverseTheNumber(int number, int amountOfDigitsInNumber, int[] numbersDigits) {
        if (amountOfDigitsInNumber == 1) {
            numbersDigits[0] = number;
            return numbersDigits;
        }
        numbersDigits[amountOfDigitsInNumber - 1] = (int) (number / pow(DETERMINANT, amountOfDigitsInNumber - 1));
        int residue = (int) (number % pow(DETERMINANT, amountOfDigitsInNumber - 1));
        reverseTheNumber(residue, amountOfDigitsInNumber - 1, numbersDigits);
        return numbersDigits;
    }
}