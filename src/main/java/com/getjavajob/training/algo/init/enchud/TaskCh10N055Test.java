package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N055.convertNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {

    public static void main(String[] args) {
        firstTestConvertNumbers();
    }

    public static void firstTestConvertNumbers() {
        assertEquals("TaskCh10N055Test.testConvertNumbers", "1F", convertNumber(31, 16));
        assertEquals("TaskCh10N055Test.testConvertNumbers", "1010", convertNumber(10, 2));
    }
}