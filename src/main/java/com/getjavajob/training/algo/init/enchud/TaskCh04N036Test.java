package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh04N036.checkColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {

    public static void main(String[] args) {
        testFirstCheckColor();
        testSecondCheckColor();
    }

    private static void testSecondCheckColor() {
        assertEquals("TaskCh04N036Test.testFirstCheckColor", true, checkColor(5));
    }

    public static void testFirstCheckColor() {
        assertEquals("TaskCh04N036Test.testFirstCheckColor", false, checkColor(3));
    }
}