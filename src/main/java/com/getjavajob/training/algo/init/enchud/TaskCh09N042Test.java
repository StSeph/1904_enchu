package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N042.reverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {

    public static void main(String[] args) {
        testReverseWord();
    }

    public static void testReverseWord() {
        assertEquals("TaskCh09N042Test.testReverseWord", "olleh", reverseWord("hello"));
    }
}