package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N042 {

    public static void main(String[] args) {
        Scanner inputLine = new Scanner(System.in);
        String enteredWord = inputLine.nextLine();
        String reversedWord = reverseWord(enteredWord);
        System.out.println(reversedWord);
    }

    public static String reverseWord(String enteredWord) {
        return new StringBuilder(enteredWord).reverse().toString();
    }
}
