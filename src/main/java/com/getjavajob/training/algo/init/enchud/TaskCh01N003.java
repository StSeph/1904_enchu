package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh01N003 {

    public static void main(String[] args) {
        System.out.println("Enter the number");
        Scanner inputNumber = new Scanner(System.in);
        int number = inputNumber.nextInt();
        System.out.println("You entered " + number);
    }
}