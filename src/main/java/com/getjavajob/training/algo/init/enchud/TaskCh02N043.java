package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh02N043 {

    public static void main(String[] args) {

        Scanner inputNumber = new Scanner(System.in);
        System.out.println("Enter the first number");
        int a = inputNumber.nextInt();
        System.out.println("Enter the second number");
        int b = inputNumber.nextInt();
        boolean divide = areDivided(a, b);
        System.out.println(divide ? "1" : "2");
    }

    public static boolean areDivided(int a, int b) {
        if ((a == 0 & b != 0) || (a != 0 && b == 0)) {
            return true;
        } else {
            return a % b == 0 || b % a == 0;
        }
    }
}