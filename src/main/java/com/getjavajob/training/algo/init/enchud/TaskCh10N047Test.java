package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N047.countNumberFibonacci;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {

    public static void main(String[] args) {
        testCountNumberFibonacci();
    }

    public static void testCountNumberFibonacci() {
        assertEquals("TaskCh10N047Test.testCountNumberFibonacci", 34, countNumberFibonacci(9));
    }
}