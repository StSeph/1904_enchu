package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh12N028 {

    public static void main(String[] args) {
        System.out.println("Select array size. Size must be even");
        Scanner inputNumber = new Scanner(System.in);
        int size = inputNumber.nextInt();
        if (size % 2 == 1) {
            int[][] snakedArray = fillArrayWithSnake(size);
            for (int i = 0; i < snakedArray.length; i++) {
                for (int j = 0; j < snakedArray.length; j++) {
                    System.out.print(snakedArray[i][j]);
                    System.out.print(" ");
                }
                System.out.println();
            }
        } else {
            System.out.println("The size must be even. Restart programme, please");
        }
    }

    public static int[][] fillArrayWithSnake(int size) {
        int[][] emptyArray = new int[size][size];
        int valOfElement = 1;
        int stage = 1;
        for (int i = 0; i / 2 - 1 < emptyArray.length; i++) {
            if (i % 4 == 0) {
                for (int j = 0; j + stage / 2 < emptyArray.length; j++, valOfElement++) {
                    emptyArray[stage / 4][j + stage / 4] = valOfElement;
                }
            } else if (i % 4 == 1) {
                for (int k = 0; k + stage / 2 < emptyArray.length; k++, valOfElement++) {
                    emptyArray[k + stage / 4 + 1][emptyArray.length - 1 - stage / 4] = valOfElement;
                }
            } else if (i % 4 == 2) {
                for (int m = 0; m + stage / 2 < emptyArray.length; m++, valOfElement++) {
                    emptyArray[emptyArray.length - 1 - stage / 4][emptyArray.length - 2 - m - stage / 4] = valOfElement;
                }
            } else {
                for (int h = 0; h + stage / 2 < emptyArray.length; h++, valOfElement++) {
                    emptyArray[emptyArray.length - 1 - h - stage / 4][stage / 4 - 1] = valOfElement;
                }
            }
            stage++;
        }
        return emptyArray;
    }
}