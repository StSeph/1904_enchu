package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh09N015 {

    public static void main(String[] args) {
        System.out.println("Write the word");
        Scanner inputLine = new Scanner(System.in);
        String enteredWord = inputLine.nextLine();
        System.out.println("What is the letter number of word to display?");
        Scanner inputNumber = new Scanner(System.in);
        int numberOfLetter = inputNumber.nextInt();
        char letterFromWord = findLetter(enteredWord, numberOfLetter);
        System.out.println(letterFromWord);
    }

    public static char findLetter(String enteredWord, int numberOfLetter) {
        return enteredWord.charAt(numberOfLetter - 1);
    }
}