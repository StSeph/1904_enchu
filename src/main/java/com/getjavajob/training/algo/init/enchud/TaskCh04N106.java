package com.getjavajob.training.algo.init.enchud;

import java.util.Scanner;

public class TaskCh04N106 {

    public static void main(String[] args) {
        System.out.println("Enter number of month");
        Scanner inputNumber = new Scanner(System.in);
        int month = inputNumber.nextInt();
        String season = determineSeason(month);
        if (month >= 1 && month <= 12) {
            System.out.println("Now is a/an " + season);
        } else {
            System.out.println("You entered wrong number of month");
        }
    }

    public static String determineSeason(int month) {
        String seasonNow = "";
        switch (month) {
            case 1:
            case 2:
            case 12:
                seasonNow = "winter";
                break;
            case 3:
            case 4:
            case 5:
                seasonNow = "spring";
                break;
            case 6:
            case 7:
            case 8:
                seasonNow = "summer";
                break;
            case 9:
            case 10:
            case 11:
                seasonNow = "autumn";
                break;
            default:
                seasonNow = "You entered wrong number of month";
        }
        return seasonNow;
    }
}