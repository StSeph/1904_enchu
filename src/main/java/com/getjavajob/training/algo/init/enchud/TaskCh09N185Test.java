package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh09N185.checkBracesInFormula;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {

    public static void main(String[] args) {
        testCheckFormulaOne();//  (((x - 3) + (4 - 2))*2 - 3*(2 - 1)) - for the first test we insert this formula. Everything is normal here
        testCheckFormulaTwo();//  ((x - 2)+(2 - 1)*7) - 3)) - there are 2 extra right brackets. It should give out that the first extra bracket starts from the position
        testCheckFormulaThree();//   (((x - 3)/2 +2) + ((z-2)*2  - there are 2 extra left brackets. Must issue that you have two extra brackets
    }

    public static void testCheckFormulaOne() {
        assertEquals("TaskCh09N185Test.testCheckFormulaOne", "There is all correct!", checkBracesInFormula("(((x - 3) + (4 - 2))*2 - 3*(2 - 1))"));
    }

    public static void testCheckFormulaTwo() {
        assertEquals("TaskCh09N185Test.testCheckFormulaTwo", "First extra right bracket on position 23", checkBracesInFormula("((x - 2)+(2 - 1)*7) - 3))"));
    }

    public static void testCheckFormulaThree() {
        assertEquals("TaskCh09N185Test.testCheckFormulaThree", "You have extra brace(s). There is(are) - 2", checkBracesInFormula("(((x - 3)/2 +2) + ((z-2)*2"));
    }
}