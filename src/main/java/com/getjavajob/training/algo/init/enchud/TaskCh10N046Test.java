package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N046.countStepNumber;
import static com.getjavajob.training.algo.init.enchud.TaskCh10N046.countSumOfNumbers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {

    public static void main(String[] args) {
        testCountStepNumber();
        testCountSumOfNumbers();
    }

    public static void testCountStepNumber() {
        assertEquals("TaskCh10N045Test.testCountStepNumber", 1024, countStepNumber(4, 4, 4));
    }

    public static void testCountSumOfNumbers() {
        assertEquals("TaskCh10N045Test.testCountSumOfNumbers", 1364, countSumOfNumbers(4, 4, 4));
    }
}