package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh04N033.isOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testIsOdd();
        testIsEven();
    }

    private static void testIsEven() {
        assertEquals("TaskCh04N033Test.testIsOdd", false, isOdd(4));
    }

    public static void testIsOdd() {
        assertEquals("TaskCh04N033Test.testIsOdd", true, isOdd(7));
    }
}