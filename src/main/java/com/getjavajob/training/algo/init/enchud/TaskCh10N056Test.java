package com.getjavajob.training.algo.init.enchud;

import static com.getjavajob.training.algo.init.enchud.TaskCh10N056.isPrime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {

    public static void main(String[] args) {
        testIsPrime();
        testNotPrime();
    }

    public static void testNotPrime() {
        assertEquals("TaskCh10N056Test.testIsOdd", false, isPrime(10));
    }

    public static void testIsPrime() {
        assertEquals("TaskCh10N056Test.testIsOdd", true, isPrime(7));
    }
}