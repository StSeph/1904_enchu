package com.getjavajob.training.algo.init.enchud;

public class TaskCh10N056 {
    public static void main(String[] args) {

        System.out.println(isPrime(11) ? "Your number is prime" : "Your number isn't prime");
    }

    public static boolean isPrime(int number) {
        return checkNumberForPrime(number, number - 1);
    }

    private static boolean checkNumberForPrime(int number, int divider) {
        if (number > 1) {
            if (divider == 1) {
                return true;
            } else if (number % divider == 0) {
                return false;
            } else {
                return checkNumberForPrime(number, divider - 1);
            }
        }
        return false;
    }
}